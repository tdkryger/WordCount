﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Lib.WordCounter
{
    public class WordCounter
    {
        #region Fields
        private readonly List<WordCount> wordCountList = new List<WordCount>();
        private readonly object wordCountListLockObject = new object();
        #endregion

        #region Public mehtods
        public Dictionary<string, int> CountWords(string pathAndFilename)
        {
            if (!File.Exists(pathAndFilename))
                throw new FileNotFoundException($"The file {pathAndFilename} was not found.");


            var lines = File.ReadAllLines(pathAndFilename);
            Parallel.ForEach(lines, FindWordsInLine);

            var distinctWords = wordCountList.Distinct().ToList();

            var resultList = new Dictionary<string, int>();
            var lockObject = new object();

            Parallel.ForEach(distinctWords, (word) =>
            {
                if (word != null)
                {
                    word.Count = wordCountList.Count(x => x.Word == word.Word);
                    lock (lockObject)
                    {
                        if ( !resultList.ContainsKey(word.Word))
                            resultList.Add(word.Word, word.Count);
                    }
                }
            });
            return resultList.OrderBy(x => x.Value).ToDictionary(x => x.Key, x => x.Value);
        }
        #endregion

        #region Private methods
        private void FindWordsInLine(string line)
        {
            if (!string.IsNullOrEmpty(line))
            {
                var words = line.Split(' ');
                Parallel.ForEach(words, (word) =>
                {
                    if (word.EndsWith(".", StringComparison.Ordinal) || word.EndsWith(",", StringComparison.Ordinal))
                        word = word.Substring(0, word.Length - 2);
                    lock (wordCountListLockObject)
                    {
                        wordCountList.Add(new WordCount(word));
                    }
                });
            }
        }
        #endregion
    }
}
