﻿namespace Lib.WordCounter
{
    class WordCount
    {
        #region Properties
        public string Word { get; }
        public int Count { get; set; }
        #endregion

        #region Constructor
        public WordCount(string word, int count = 1)
        {
            this.Word = word;
            this.Count = count;
        }
        #endregion
    }
}
