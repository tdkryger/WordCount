﻿using Lib.WordCounter;
using System;

namespace WordCount
{
    class Program
    {
        static void Main(string[] args)
        {
            var wordCounter = new WordCounter();
            var resultList = wordCounter.CountWords(@"randomtext_lorem_p.txt");
            foreach(var keyValuePair in resultList)
            {
                Console.WriteLine($"{keyValuePair.Value}: {keyValuePair.Key}");
            }
            Console.ReadLine();
        }
    }
}
