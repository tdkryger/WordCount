﻿using System;
using System.IO;
using Lib.WordCounter;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTest.WordCount
{
    [TestClass]
    public class UtWordCounter
    {
        [TestMethod]
        [ExpectedException(typeof(FileNotFoundException))]
        public void Load_Not_Existing_File()
        {
            var wordCounter = new WordCounter();
            var resultList = wordCounter.CountWords(@"x:\doesnotexists.txt");
        }

        [TestMethod]
        public void Load_Existing_File()
        {
            // Random text from http://www.randomtext.me/#/lorem/p-18/20-87
            var wordCounter = new WordCounter();
            var resultList = wordCounter.CountWords(@"randomtext_lorem_p.txt");

            Assert.AreNotEqual(0, resultList.Count);
        }
    }
}
